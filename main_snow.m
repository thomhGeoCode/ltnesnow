% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2024 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2024) A local thermal non-equilibrium model for 
% Rain-on-Snow events

clear all
close all
clc

%% Add path
addpath('./Richards')
addpath('./heatTransfer/')

%%
% Simulation specific
% length of column (m)
height = 0.5; 
% number of grid points (-)
nz = 51;
% grid spacing (m)
dz = height / (nz-1);
% total length of simulation (s)
maxTime = 24*60.0*60.0+1.0;
% simulation time step (s)
dt = 0.01;
% time (s)
time = 0.0;
% time counting
seconds = 0.0;
minutes = 0;
hours = 0;
time_count = 0;
% thermal boundary conditions
airTemp = 273.15 - 3.0;
rainTemp = 273.15 + 4.0;
soilTemp = 273.15;

% Initial porosity of snow (-)
porosityIni = 0.4;
% Porosity of snow (-)
Porosity = porosityIni .* ones(nz,1);
% Hydraulic conductivity initial [m/s]
hydConducIni = 1e-4;
% Hydraulic conductivity [m/s]
HydConducSat = hydConducIni .* ones(nz,1);
% residual water content [-]
waterContRes = 0.02;
% residual air content [-]
airContRes = Porosity - 0.9*Porosity;
% Snow Grain Radius (m)
GrainRadius = 0.001 .* ones(nz,1);
% vanGenuchten parameter
vanGenAlpha = 7.3.*2.0.*max(2.0.*GrainRadius) + 1.9;%1.3;
vanGenN = -3.3.*2.0.*max(2.0.*GrainRadius) + 14.4;%1.25;
vanGenM = 1.0 - (1.0/vanGenN);

%% Initialize arrays
% volume fraction liquid
VolFracL = waterContRes .* ones(nz,1) + 0.01;
% Saturated water content
WaterContSat = Porosity - airContRes;
% effective saturation [-]
EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
% pressure head [m]
PressureHead = - (EffSat.^(-1.0/vanGenM) - 1.0).^(1.0/vanGenN) ./ vanGenAlpha;
% water content [-]
VolFracL = calcWaterContent(PressureHead, waterContRes, ...
  WaterContSat, vanGenAlpha, vanGenN, vanGenM);
% effective hydraulic conductivity [m/s]
EffHydConduc = calcEffHydConduc(EffSat, HydConducSat, vanGenM);
% moisture capacity (1/m)
MoistCapac = calcMoistCapac(EffSat, waterContRes, WaterContSat,...
  vanGenAlpha, vanGenM);
% flow velocity (m/s)
Vel = calcDarcyVel(EffHydConduc, PressureHead, dz);
%
VolFracA = Porosity - VolFracL;
VolFracI = 1.0 - Porosity;
% source/sink term in Richards equ. (1/s)
ChangeL = 0.0 .* PressureHead;
ChangeLm = 0.0.*ChangeL;
ChangeLf = 0.0.*ChangeL;

pressureTop = 0.1;%PressureHead(1);
pressureBottom = PressureHead(end);

%% Heat parameters
% density of water (kg/m^3)
densL = 1000.0;
% density of air (kg/m^3)
densA = 1.2;
% density of ice (kg/m^3)
densI = 1000.0;%940.0;
% heat capacity of liquid water (J/kg/K)
heatCapacL = 4200.0;
% heat capacity of air (J/kg/K)
heatCapacA = 1008.0;
% heat capacity of ice (J/kg/K)
heatCapacI = 2040.0;
% thermal conductitity of liquid water (W/m/K)
thermConducL= 0.55;
% thermal conductivity of air (W/m/K)
thermConducA = 0.024;
% thermal conductivity of ice (W/m/K)
thermConducI = 2.2;

% temperature of liquid water (K)
TempL = 273.15.*ones(nz,1);
% temperature of air (K)
TempA = linspace(airTemp, soilTemp, nz)';
% temperature of ice (K)
TempI = linspace(airTemp, soilTemp, nz)';

% Specific surface area (1/m)
A = zeros(nz,1);
% heat transfer coefficient
h = 0.1;
% latent heat (J/kg)
latentHeat = 334.0e3;

% Savings
svidx = 1;
saveTi = zeros(nz, 2*floor(maxTime/3600)+1);
saveTl = saveTi;
saveTa = saveTi;
saveVfI = saveTi;
saveVfL = saveTi;
saveVfA = saveTi;

saveTi(:,svidx) = TempI;
saveTl(:,svidx) = TempL;
saveTa(:,svidx) = TempA;
saveVfI(:,svidx) = VolFracI;
saveVfL(:,svidx) = VolFracL;
saveVfA(:,svidx) = VolFracA;

halfh = 0;
svidx = svidx + 1;

global pcLimit
pcLimit = 0.01;

%% Start the simulation
while time < maxTime

  time_count = time_count + 1;
  
  %% flow model
  EffHydConduc = calcEffHydConduc(EffSat, HydConducSat, vanGenM);
  
  MoistCapac = calcMoistCapac(EffSat, waterContRes, WaterContSat,...
    vanGenAlpha, vanGenM);

  PressureHead = calcPressureHead1d(PressureHead, EffHydConduc, ...
    MoistCapac, ChangeL, dt, dz);
  
  PressureHead(1) = pressureTop;
  PressureHead(end) = PressureHead(end-1);
  
  Vel = calcDarcyVel(EffHydConduc, PressureHead, dz);
  
  VolFracL = calcWaterContent(PressureHead, waterContRes, ...
    WaterContSat, vanGenAlpha, vanGenN, vanGenM);
  
  EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
  
  VolFracA = Porosity - VolFracL;

  %% Heat transport
  AlphaL = dt ./ ( VolFracL .* densL .* heatCapacL);
  AlphaA = dt ./ ( VolFracA .* densA .* heatCapacA);
  AlphaI = dt ./ ( VolFracI .* densI .* heatCapacI);

  % Air
  TempA = nonLinTempDiff1dPartial(TempA, VolFracA, AlphaA,...
    thermConducA, dz);
  TempA(1) = airTemp;
  TempA(end) = soilTemp;
  
  TempA = advectionUpwind3rdOrder1d(TempA, Vel, dt, dz);
  TempA(1) = airTemp;
  TempA(end) = soilTemp;
  
  % Liquid water
  TempL = nonLinTempDiff1dPartial(TempL, VolFracL, AlphaL,...
    thermConducL, dz);
  TempL(1) = rainTemp;
  TempL(end) = soilTemp;

  TempL = advectionUpwind3rdOrder1d(TempL, Vel, dt, dz);
  TempL(1) = rainTemp;
  TempL(end) = soilTemp;

  % Ice
  TempI = nonLinTempDiff1dPartial(TempI, Porosity, AlphaI,...
    thermConducI, dz);
  TempI(1) = TempI(1) - AlphaI(1) * h * VolFracL(1) / Porosity(1) * A(1) * (TempI(1) - TempL(1));%airTemp;
  TempI(end) = soilTemp;
  
  %% Heat transfer
  A = 6.0 * (1.0 - Porosity) ./ ( 2.0 .* GrainRadius );
  % ice - liquid water
  % By definition TempL > TempI but due to overshoot at Tphasechange it 
  % might occur otherwise
  Qil = h .* VolFracL ./ Porosity .* A .* EffSat .* (TempL - TempI);
  Qil(Qil<=0.0) = 0.0;
  TempL = TempL - AlphaL .* Qil;
  TempI = TempI + AlphaI .* Qil;
  % ice - air
  Qia = h .* VolFracA ./ Porosity .* A .* (1.0 - EffSat) .* (TempI - TempA);
  TempA = TempA + AlphaA .* Qia;
  TempI = TempI - AlphaI .* Qia;

  % Evtl. limit Q?!
  %% Phase change
  ChangeLm = 0.0 .* ChangeLm;
  ChangeLf = 0.0 .* ChangeLf;
  % Freezing
  if (min(nonzeros(TempL)) < (273.15-pcLimit))
    % Volumenanteil, der gefriert & Temperaturkorrektur
    [TempL, TempI, VolFracI, VolFracL, ChangeLf] = freezing(VolFracI, VolFracL, ...
      TempL, TempI, latentHeat, densL, densI, heatCapacL, waterContRes, dt);
    % Change in Grain Radius
    GrainRadius = nthroot(VolFracI./(1.0 - Porosity), 3) .* GrainRadius;
    % Change in Porosity
    Porosity = 1.0 - VolFracI;
    % Cozeny Karman for change in hydraulic conductivity
    HydConducSat = CozenyKarman(hydConducIni, porosityIni, Porosity);
    % Change in water saturation
    WaterContSat = Porosity - airContRes;
    EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
  end

%   Melting
  if (max(TempI) > (273.15+pcLimit))
    % Volumenanteil, der schmilzt & Temperaturkorrektur
    [TempL, TempI, VolFracI, VolFracL, ChangeLm] = melting(VolFracI, VolFracL,...
      TempL, TempI, latentHeat, densL, densI, heatCapacI, dt);
    % Change in Grain Radius
    GrainRadius = nthroot(VolFracI./(1.0 - Porosity), 3) .* GrainRadius;
    % Change in Porosity
    Porosity = 1.0 - VolFracI;
    % Cozeny Karman for change in hydraulic conductivity
    HydConducSat = CozenyKarman(hydConducIni, porosityIni, Porosity);
    % Change in water saturation
    WaterContSat = Porosity - airContRes;
    EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
  end
  ChangeL = ChangeLm + ChangeLf;

  %% housekeeping
  time = time + dt;
  
  seconds = seconds + dt;
  
  if (seconds >= 60.0)
    minutes = minutes + 1;
    seconds = seconds - 60.0;
    halfh = halfh+1;
    if (halfh>=30)
      halfh = 0;
    end
    if (minutes >=60)
      hours = hours + 1;
      minutes = 0;
    end
  end

  %% saving
  if ( seconds < dt && halfh == 0 )
    saveTi(:,svidx) = TempI;
    saveTl(:,svidx) = TempL;
    saveTa(:,svidx) = TempA;
    saveVfI(:,svidx) = VolFracI;
    saveVfL(:,svidx) = VolFracL;
    saveVfA(:,svidx) = VolFracA;
    
    svidx = svidx + 1;
  end
end

save('results.mat')

%%
figure()
hold on
for i=2:size(saveTi,2)-1
  plot(saveVfL(:,i) - saveVfL(:,1),'b')
  plot(saveVfI(:,i) - saveVfI(:,1),'c')
end
hold off
ylabel('change in volume fraction (-)')
xlabel('distance along column (cm)')
legend('liquid','ice')

%%
figure()
hold on
for i=2:size(saveTi,2)
  plot(saveTl(:,i) - saveTl(:,1),'b')
  plot(saveTi(:,i) - saveTi(:,1),'c')
end
hold off
ylabel('temperature change (°C)')
xlabel('distance along column (cm)')
legend('liquid','ice')

%%
figure()
hold on
for i=1:size(saveTi,2)
  plot(saveTl(:,i)-273.15,'b')
  plot(saveTi(:,i)-273.15,'c')
end
hold off
ylabel('temperature (°C)')
xlabel('distance along column (cm)')
legend('liquid','ice')

%%
z = 0:dz:height;

%%
figure()
tiledlayout(1,3);
nexttile
hold on
plot(saveVfI(:,1),-z)
plot(saveVfI(:,12),-z)
plot(saveVfI(:,24),-z)
hold off
box on
xlim([0.45 0.65])
leg=legend('t=0h','t=12h','t=24h','Location','southwest');
leg.ItemTokenSize = [20,10];
xticks([0.4 0.45 0.5 0.55 0.6 0.65])
ylabel('depth of snow column (m)')
xlabel('volumetric ice content (-)')

nexttile
hold on
plot(saveTl(:,1),-z)
plot(saveTl(:,12),-z)
plot(saveTl(:,24),-z)
hold off
box on
xlim([273 277.25])
xticks([273 274 275 276 277])
xlabel('liquid water temperature (K)')

nexttile
hold on
plot(saveTi(:,1),-z)
plot(saveTi(:,12),-z)
plot(saveTi(:,24),-z)
hold off
box on
xlim([270 273.25])
xticks([270 271 272 273])
xtickangle(45)
xlabel('ice temperature (K)')

%%
figure()
tiledlayout(1,3);
nexttile
hold on
plot(saveVfI(:,1),-z)
plot(saveVfI(:,12),-z)
plot(saveVfI(:,24),-z)
hold off
box on
% xlim([0.45 0.65])
ylim([-0.5 -0.09])
leg=legend('t=0h','t=12h','t=24h','Position',[0.227 0.147 0.1 0.15]);
leg.ItemTokenSize = [20,10];
% xticks([0.4 0.45 0.5 0.55 0.6 0.65])
ylabel('depth of snow column (m)')
xlabel('volumetric ice content (-)')

nexttile
hold on
plot(saveTl(:,1),-z)
plot(saveTl(:,12),-z)
plot(saveTl(:,24),-z)
hold off
box on
xlim([273.09 273.21])
ylim([-0.5 -0.09])
% xticks([273 274 275 276 277])
xlabel('liquid water temperature (K)')

nexttile
hold on
plot(saveTi(:,1),-z)
plot(saveTi(:,12),-z)
plot(saveTi(:,24),-z)
hold off
box on
xlim([273.08 273.18])
ylim([-0.5 -0.09])
% xticks([270 271 272 273])
% xtickangle(45)
xlabel('ice temperature (K)')

%%
figure()
tiledlayout(1,3);
nexttile
plot(GrainRadius, -z)
xlabel('grain radius (m)')
ylabel('depth of water column (m)')

nexttile
plot(HydConducSat,-z)
xlabel('hydr conduc (m/s)')