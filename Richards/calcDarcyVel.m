% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function Vel = calcDarcyVel(EffHydConduc, PressureHead, dz)

  Vel = 0.0 .* PressureHead;

  Vel(1:end-1) = - EffHydConduc(1:end-1) .* ( ...
    (PressureHead(2:end) - PressureHead(1:end-1) ) ./ dz );
  
  Vel(end) = Vel(end-1);

end