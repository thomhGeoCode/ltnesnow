% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function EffHydConduc = calcEffHydConduc(EffSat, SatHydConduc, vanGenM)

  EffHydConduc = sqrt( EffSat ) .* SatHydConduc .* ...
                 ( 1.0 - abs( 1.0 - EffSat.^(1.0./vanGenM) ).^vanGenM ).^2.0;

end