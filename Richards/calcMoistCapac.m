% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function MoistCapac = calcMoistCapac(EffSat, waterContRes, WaterContSat,...
  vanGenAlpha, vanGenM)

  % NOTE: MoistCapac > 0 if EffSat < 0
  MoistCapac = 1.0 .* vanGenAlpha .* vanGenM .* ...
               ( WaterContSat - waterContRes ) ./ ( 1.0 - vanGenM ) .* ...
               EffSat.^(1.0./vanGenM) .* ...
               ( 1.0 - EffSat.^(1.0./vanGenM) ).^vanGenM;
  
  MoistCapac(EffSat>=0.999) = 1.0;
  
end