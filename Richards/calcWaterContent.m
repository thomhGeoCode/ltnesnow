% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function WaterCont = calcWaterContent(PressureHead, waterContRes, ...
  WaterContSat, vanGenAlpha, vanGenN, vanGenM)

  WaterCont = waterContRes + (WaterContSat - waterContRes) ./ ...
              ( 1.0 + ( vanGenAlpha .* abs( PressureHead ) ) ...
              .^vanGenN ).^vanGenM;

  WaterCont(PressureHead >= 0) = WaterContSat(PressureHead >= 0);
end