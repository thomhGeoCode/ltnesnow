% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2024 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2024) A local thermal non-equilibrium model for 
% Rain-on-Snow events

function [Ksat] = CozenyKarman(Kini, phiini, phi)

  Ksat = Kini .* ( (phi.*phi.*phi) .* ( 1.0 - phiini).^2 ) ./ ...
    ((phiini.*phiini.*phiini) .* ( 1.0 - phi).^2 );