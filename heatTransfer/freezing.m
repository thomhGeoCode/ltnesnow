% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2024 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2024) A local thermal non-equilibrium model for 
% Rain-on-Snow events

function [Tliquid, Tice, VolFracI, VolFracL, ChangeL] = freezing(VolFracI, VolFracL, ...
  Tliquid, Tice, latentHeat, densL, densI, heatCapacL, waterContRes, dt)

  global pcLimit

  TphaseChange = 273.15;

  % Calculate available energy due to underestimated temperature
  DeltaT = TphaseChange - Tliquid;
  DeltaT(DeltaT<pcLimit) = 0.0;
  DeltaT(VolFracL<=waterContRes) = 0.0;
  
  % Calculate available energy
  Q = VolFracL .* densL .* heatCapacL .* DeltaT;
  
  % Calculate change in liquid water volume fraction due to freezing
  ChangeVolFracL = Q ./ densL ./ latentHeat;  

  % Updated liquid water content
  VolFracLNew = VolFracL - ChangeVolFracL;
  VolFracL(VolFracLNew<=waterContRes) = waterContRes(VolFracLNew<=waterContRes);
  VolFracL(VolFracLNew>waterContRes) = VolFracLNew(VolFracLNew>waterContRes);
  ChangeVolFracL(VolFracLNew <= waterContRes) = VolFracL(VolFracLNew <= waterContRes) - waterContRes(VolFracLNew <= waterContRes);
  clear VolFracLNew

  % Calculate change in ice volume fraction due to freezing
  ChangeVolFracI = ChangeVolFracL .* densL ./ densI;
  
  % Ice temperature increased due warmer newly frozen water
  Tice(DeltaT > 0) = min(TphaseChange, ...
    (VolFracI(DeltaT > 0) .* Tice(DeltaT > 0) + ...
    ChangeVolFracI(DeltaT > 0) .* TphaseChange) ./ ...
    ( VolFracI(DeltaT > 0) + ChangeVolFracI(DeltaT > 0) ));
  
  % Set liquid water temperature
  Tliquid(DeltaT > 0) = TphaseChange;
  
  % Updated Ice water content
  VolFracI = VolFracI + ChangeVolFracI;
  
  % Changing rate (1/s)
  ChangeL = - ChangeVolFracL./dt;

end