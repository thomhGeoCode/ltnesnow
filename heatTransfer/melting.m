% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2024 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2024) A local thermal non-equilibrium model for 
% Rain-on-Snow events

function [Tliquid, Tice, VolFracI, VolFracL, ChangeL] = melting(VolFracI, VolFracL, ...
  Tliquid, Tice, latentHeat, densL, densI, heatCapacI, dt)

global pcLimit

TphaseChange = 273.15;

% Calculate available energy due to overestimated temperature
DeltaT = Tice - TphaseChange;
DeltaT(DeltaT<pcLimit) = 0.0;
DeltaT(VolFracI<=0.2) = 0.0;

% Calculate available energy
Q = VolFracI .* densI .* heatCapacI .* DeltaT;
  
% Calculate change in liquid water volume fraction due to freezing
ChangeVolFracI = Q ./ densI ./ latentHeat;

VolFracINew = VolFracI - ChangeVolFracI;
VolFracI(VolFracINew<=0.2) = 0.2;
VolFracI(VolFracINew>0.2) = VolFracINew(VolFracINew>0.2);
ChangeVolFracI(VolFracINew<=0.2) = VolFracI(VolFracINew<=0.2) - 0.2;
clear VolFracINew

% Calculate change in ice volume fraction due to freezing
ChangeVolFracL = ChangeVolFracI .* densI ./ densL;

% Set ice temperature
Tice(DeltaT > 0) = TphaseChange;

Tliquid(DeltaT > 0) = max(TphaseChange, ...
  (VolFracL(DeltaT > 0) .* Tliquid(DeltaT > 0) + ...
  ChangeVolFracL(DeltaT > 0) .* TphaseChange) ./ ...
  ( VolFracL(DeltaT > 0) + ChangeVolFracL(DeltaT > 0) ));

% Updated liquid water content
VolFracL = VolFracL + ChangeVolFracL;
  
% Changing rate (1/s)
ChangeL = ChangeVolFracL./dt;