% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function NewValue = nonLinTempDiff1dPartial(Value, VolFrac, Alpha,...
  thermalConduc, dz)

  NewValue = Value;
  
  for i=2:length(Value)-1
    NewValue(i) = Value(i) + Alpha(i) * ( ...
      ( VolFrac(i+1) + VolFrac(i) ) * 0.5 * thermalConduc * ...
      ( Value(i+1) - Value(i) ) - ...
      ( VolFrac(i-1) + VolFrac(i) ) * 0.5 * thermalConduc * ...
      ( Value(i) - Value(i-1) ) ) /dz /dz;
  end

end